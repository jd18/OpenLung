# Overview: 
**TMC5160A chipset based Stepper motor control up to 60V and 2.8A per phase.**
---
## Scope:
- Controlling a Stepper motor via SPI interface

## Designer:
Nadim Conti (Open Source Ventilator - Velocity Team )

## Stackup
4 Layer PCB

## Designed with:
**Altium Designer**

## Source/previous design:
https://www.trinamic.com/support/eval-kits/details/tmc5160-bob/

## Changelog:
- Updated Mosfets to 60V rated ones (CSD88537ND from Texas Instruments)
- Mind that TMC5160 must be updated to TMC5160A while placing order.

## Known Issues:
- Voltage on stepper motor is now limited by both MOSFETs and TMC5160A
